package com.example.gallaryapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.view.FrameStats;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import android.app.DownloadManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import static androidx.core.app.ActivityCompat.requestPermissions;
import static androidx.core.content.ContextCompat.checkSelfPermission;

public class myadapter extends RecyclerView.Adapter<myadapter.myviewholder>  implements ActivityCompat.OnRequestPermissionsResultCallback
{
    FatchingDatum []data;
    Context context;
    Activity contxt;
    private  final int PERMISSION_Storage_code=1000;
    private String downloadUrl;

    public myadapter(FatchingDatum[] data, Context context,Activity contxt) {
        this.data = data;
        this.context = context;
        this.contxt = contxt;
    }

    @NonNull
    @Override
    public myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View view=inflater.inflate(R.layout.singlerow,parent,false);
        return new myviewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull myviewholder holder, int position)
    {
         FatchingDatum datum=data[position];
         holder.tv.setText(datum.getName());
//        +datum.getImage()
        Glide.with(holder.img.getContext()).load("http://gallaryappproject.000webhostapp.com/storage/../assets/img/db_images/"+datum.getImage()).into(holder.img);
        System.out.print("Hello JAY"+datum.getImage());
        holder.downloadbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadUrl="http://gallaryappproject.000webhostapp.com/storage/../assets/img/db_images/"+datum.getImage();
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
                {
                    if (checkSelfPermission(contxt,Manifest.permission.WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_DENIED)
                    {
                        String[] permissions= {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                        requestPermissions((Activity)contxt,permissions,PERMISSION_Storage_code);
                    }
                    else
                    {
                        startDownloading(downloadUrl);
                    }
                }
                else {

                    startDownloading(downloadUrl);
                }


            }
        });

    }
    private void startDownloading(String downloadUrl) {
        System.out.println(downloadUrl);
        DownloadManager.Request request= new DownloadManager.Request(Uri.parse(downloadUrl));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setTitle("Download");
        request.setDescription("Downloading Image...");
        request.setMimeType("image/jpg");
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,""+System.currentTimeMillis()+"."+"jpg");

        DownloadManager manager=(DownloadManager)  contxt.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PERMISSION_Storage_code:{
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    startDownloading(downloadUrl);
                }
                else{
                    Toast.makeText(context,"Permission Denied!",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return data.length;
    }



    public class myviewholder extends RecyclerView.ViewHolder
       {
           ImageView img;
           TextView tv;
           Button downloadbtn;
           public myviewholder(@NonNull View itemView) {
               super(itemView);
               img = (ImageView)itemView.findViewById(R.id.imageholder);
               tv = (TextView)itemView.findViewById(R.id.theader);
               downloadbtn =(Button) itemView.findViewById(R.id.downloadbtn);
           }
       }
}
