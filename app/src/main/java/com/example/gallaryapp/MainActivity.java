package com.example.gallaryapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Button UploadBtn,DispBtn,downloadBtn;
    private EditText Name;
    private ImageView imgView;
    FloatingActionButton chooseimg;
    FloatingActionButton camera;
    private  final int IMG_REQUEST = 1,CAPTURE_REQUEST=2,PERMISSION_Storage_code=1000;
    private Bitmap bitmap;
    private String uploadUrl="http://gallaryappproject.000webhostapp.com/api/addapi";
    private String dispUrl="http://gallaryappproject.000webhostapp.com/api/displayurl";
    RecyclerView recview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recview = (RecyclerView)findViewById(R.id.recview);
        GridLayoutManager gridLayoutManager=new GridLayoutManager(this,2);
        recview.setLayoutManager(gridLayoutManager);
        displayImage();

        UploadBtn = (Button) findViewById(R.id.uploadbtn);
        DispBtn = (Button) findViewById(R.id.dispbtn);



        Name = (EditText) findViewById(R.id.name);
        imgView = (ImageView) findViewById(R.id.imageview);

        // floating buttons
        camera=findViewById(R.id.camera);
        chooseimg=findViewById(R.id.img);

        camera.setOnClickListener(this);
        chooseimg.setOnClickListener(this);
        UploadBtn.setOnClickListener(this);
        DispBtn.setOnClickListener(this);
//        downloadBtn.setOnClickListener(this);

    }



    @Override
    public void onClick(View v) {
       switch (v.getId())
       {
           case R.id.img:
               selectImage();
               break;
           case R.id.uploadbtn:
               uploadImage();
               break;
           case R.id.camera:
               captureImage();
               break;
           case R.id.dispbtn:
               finish();
               startActivity(getIntent());
               break;
       }
    }

    private void captureImage()
    {
        Intent intent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(intent.resolveActivity(getPackageManager()) !=null)
        {
            startActivityForResult(intent,CAPTURE_REQUEST);
        }
    }
    private void selectImage()
    {
        Intent intent=new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,IMG_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==IMG_REQUEST  && resultCode==RESULT_OK && data!=null)
        {
            Uri path = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),path);
                imgView.setImageBitmap(bitmap);
                imgView.setVisibility(View.VISIBLE);
                Name.setVisibility(View.VISIBLE);
                UploadBtn.setVisibility(View.VISIBLE);
                DispBtn.setVisibility(View.VISIBLE);
                recview.setVisibility(View.INVISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        if(requestCode==CAPTURE_REQUEST  && resultCode==RESULT_OK && data!=null) {
            bitmap = (Bitmap) data.getExtras().get("data");
            imgView.setImageBitmap(bitmap);
            imgView.setVisibility(View.VISIBLE);
            Name.setVisibility(View.VISIBLE);
            UploadBtn.setVisibility(View.VISIBLE);
            DispBtn.setVisibility(View.VISIBLE);
            recview.setVisibility(View.INVISIBLE);
        }
    }


    private  void displayImage()
    {
        StringRequest request=new StringRequest(dispUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                GsonBuilder gsonBuilder=new GsonBuilder();
                Gson gson=gsonBuilder.create();
                FatchingDatum []data=gson.fromJson(response,FatchingDatum[].class);
                recview.setAdapter(new myadapter(data,getApplicationContext(),MainActivity.this));
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        RequestQueue queue=Volley.newRequestQueue(this);
        queue.add(request);
    }


    private void uploadImage()
    {

        StringRequest request=new StringRequest(Request.Method.POST, uploadUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    String Response = jsonObject.getString("response");
                    Toast.makeText(MainActivity.this,Response,Toast.LENGTH_LONG).show();
                    imgView.setImageResource(0);
                    imgView.setVisibility(View.GONE);
                    Name.setText("");
                    Name.setVisibility(View.GONE);
                    UploadBtn.setVisibility(View.GONE);
                    DispBtn.setVisibility(View.GONE);
                    finish();
                    startActivity(getIntent());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("name",Name.getText().toString().trim());
                params.put("upload",imagetoString(bitmap));
                return params;
            }
        };

        RequestQueue queue= Volley.newRequestQueue(getApplicationContext());
        queue.add(request);
    }

    private String imagetoString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }
}